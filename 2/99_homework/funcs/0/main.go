package main

import (
	"fmt"
	"math"
)

// TODO: Реализовать вычисление Квадратного корня
func Sqrt(x float64) float64 {
	y := x
	if x < 0 {
		return math.NaN()
	} else if x == 0 {
		return x
	}
	for i := 0; i <= 5; i++ {
		y = 0.5 * (y + x/y)
	}
	return y
}

func main() {
	fmt.Println(Sqrt(1))
}
